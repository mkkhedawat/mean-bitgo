# Pre-req
- nodejs
- yarn
- express
- mongodb

## How to run

```
cd /path/to/repo
yarn
cd www/
yarn
cd ..
npm start
# open chrome at localhost:3000
```