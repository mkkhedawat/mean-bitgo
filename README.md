# Pre-req
- nodejs
- yarn
- express
- mongodb

## How to run

```
cd /path/to/repo
yarn
cd www/
yarn
cd ..
npm start
#open chrome at localhost:3000

```

## SetUp Mail Server
Update email and pass word in server/mail/mail.js

## Test User1
username = m@n.op
pass = 12345678
wallet address : 2N6nK5ThuCCDHcgp7xmKCesfrr9NFTWyj4g

## Test User2
username = h@hello.com
pass = 12345678
wallet address : 2N6Bmcz6FtdMQfFqeXWdMaYEVVxUtiEdExP